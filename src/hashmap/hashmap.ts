type PropertyLocation = string | string[];
type Hashmap<G> = { [key: string]: G };

const getNestedValue = <G>(item: G, prop: string[]): G =>
  // Loop through each level of nested objects
  prop.reduce((acc: G, propName: string) => {
    // If property to search for exists on this level
    if (acc.hasOwnProperty(propName)) {
      // Return value at the key to search for, ending on final value
      return acc[propName];
    }
    return acc;
    // Starts with object
  }, item);

/**
 * Returns a hashmap where the key is the value at a property within an object.
 * When additional properties are provided new entries are made in
 * the hashmap which point to the same object.
 *
 * The values at each property that are used as keys should be unique.
 * If the prop points to a list, items will be made for each value.
 * Only strings and numbers are acceptable types to make keys from.
 *
 * @param input
 * @param prop
 * @param additionalProps
 */
export const hashmap = <G>(
  input: G[],
  prop: PropertyLocation,
  additionalProps?: Array<PropertyLocation>,
): Hashmap<G> => {
  const propNested = Array.isArray(prop) && prop.length > 0;
  const additionalPropsExist =
    additionalProps != null &&
    Array.isArray(additionalProps) &&
    additionalProps.length > 0;

  return input.reduce((acc: Hashmap<G>, item: G) => {
    // Get prop value
    const propValue: PropertyLocation = propNested
      ? getNestedValue(item, prop as string[])
      : item[prop as string];

    const propValueIsArray = Array.isArray(propValue) && propValue.length > 0;
    const propValueStringArray = propValueIsArray
      ? // Already an array
        (propValue as string[])
      : // Put single value in to array
        [propValue as string];
    // Build dict to add to hashmap for item
    const selectedPropDict = propValueStringArray.reduce(
      (acc: Hashmap<G>, value) => {
        // Only allow number or string values as the key
        if (typeof value !== 'string' && typeof value !== 'number')
          throw Error(
            `Value at ${JSON.stringify(
              prop,
            )} must be a number or a string. Recieved: ${JSON.stringify(
              value,
            )}`,
          );
        // If key is number or string, return hashmap
        return {
          ...acc,
          [value]: item,
        } as Hashmap<G>;
      },
      {},
    );

    // Get all values from additional props
    const additionalPropsValues = additionalPropsExist
      ? additionalProps.map((additionalProp) => {
          // If is nested
          const isNested =
            Array.isArray(additionalProp) && additionalProp.length > 0;
          // Return value from nested obj
          if (isNested) return getNestedValue(item, additionalProp as string[]);
          // Return value from top level
          return item[additionalProp as string];
        })
      : [];

    const flatAdditionalPropsValues: string[] = additionalPropsValues.reduce(
      (acc, value) => {
        return acc.concat(value, []);
      },
      [],
    );

    const additionalPropsDict = flatAdditionalPropsValues.reduce(
      (acc: Hashmap<G>, value) => {
        // Only allow number or string values as the key
        if (typeof value !== 'string' && typeof value !== 'number')
          throw Error(
            `Value at ${JSON.stringify(
              prop,
            )} must be a number or a string. Recieved: ${JSON.stringify(
              value,
            )}`,
          );
        // If key is number or string, return hashmap
        return {
          ...acc,
          [value]: item,
        } as Hashmap<G>;
      },
      {},
    );

    return {
      ...acc,
      ...selectedPropDict,
      ...additionalPropsDict,
    };
  }, {});
};
