import { fuzzyparse } from './fuzzyparse';

describe('fuzzyparse.ts', () => {
  describe('fuzzyparse', () => {
    describe('returns value from hashmap where key is string matched in text', () => {
      it('returns null with no match', () => {
        const input = {
          'konga line': {
            name: 'konga line',
            stupid: 'yes',
          },
        };
        const result = fuzzyparse(input, 'my favourite dance is the tango');
        expect(result).toEqual(null);
      });
      it('with one input', () => {
        const input = {
          'konga line': {
            name: 'konga line',
            stupid: 'yes',
          },
        };
        const result = fuzzyparse(
          input,
          'my favourite dance is the konga line',
        );
        expect(result).toEqual(input['konga line']);
      });

      it('with multiple inputs', () => {
        const input = {
          'tom delong': {
            name: 'Tom Delong',
            stupid: 'yes',
          },
          'Mark Hoppus': {
            name: 'Mark Hoppus',
            stupid: 'yes',
          },
          'Travis Barker': {
            name: 'Travis Barker',
            stupid: 'yes',
          },
        };
        const result = fuzzyparse(
          input,
          'The bass guitar player of Blink-182 was Mark Hoppus',
        );
        expect(result).toEqual(input['Mark Hoppus']);
      });
    });

    describe('can return multiple results', () => {
      it('when the top three results are requested', () => {
        const input = {
          'Hallesches Tor': {
            name: 'Hallesches Tor',
          },
          Hermannstrasse: {
            name: 'Hermannstrasse',
          },
          Hermanstrasse: {
            name: 'Hermannstrasse',
          },
        };
        const result = fuzzyparse(
          input,
          'Incident at Hermannstrasse station today',
          { numResults: 2 },
        );
        expect(result.length).toEqual(2);
      });

      it('returns as many results as is possible if numResults longer than number of matches', () => {
        const input = {
          'Hallesches Tor': {
            name: 'Hallesches Tor',
          },
          Hermannstrasse: {
            name: 'Hermannstrasse',
          },
          Hermanstrasse: {
            name: 'Hermannstrasse',
          },
        };
        const result = fuzzyparse(
          input,
          'Incident at Hermannstrasse station today',
          { numResults: 5 },
        );
        // TODO: get this to 2
        expect(result.length).toEqual(3);
      });
    });
  });
});
