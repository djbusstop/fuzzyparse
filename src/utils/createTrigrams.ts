/**
 * Creates trigrams from text input. Ignores punctuation.
 * @param text
 */
export const createTrigrams = (text: string): Array<Array<string>> => {
  // Get all words from text as list
  const wordList: Array<string> = text.split(' ').filter((word) => word);

  // If entire sentence is less than three words
  if (wordList.length <= 3) {
    return [wordList];
  }

  const trigrams: Array<Array<string>> = wordList.reduce(
    (acc, value, index, array) => {
      const nextWord: string = array[index + 1];
      const twoWordsAhead: string = array[index + 2];
      // If the word in the list is third to last
      if (nextWord && twoWordsAhead) {
        return [...acc, [value, nextWord, twoWordsAhead]];
      }
      return acc;
    },
    [],
  );
  return trigrams;
};
