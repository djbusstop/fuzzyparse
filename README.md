# `fuzzyparse`

Returns the value at a key when a fuzzy match of the key exists in text. It checks the similarity of each key in trigrams of the input text and returns the value at the highest matching keys above a defined threshold.

The library is fully typed, functions infer return types from their arguments.

There is only one external dependency: [fuzzysort](https://www.npmjs.com/package/fuzzysort).

Example:

```ts
// Input
const stations = {
  Hermannstrasse: {
    name: 'Hermannstrasse',
    coords: [12.3839, 56.8327],
  },
  'Rosenthaler Platz': {
    name: 'Rosenthaler Platz',
    coords: [12.8839, 56.37239],
  },
};

const result = fuzzyparse(stations, "I'm at Hermannstrasse right now");

// {
//   name: "Hermannstrasse",
//   coords: [12.3839, 56.8327]
// },
```

## Options

```ts
{
  // Number of results. If not specified, returns top result. If specified, returns a list.
  numResults?: number,
  // Fuzzymatch threshold. Default: -6000
  matchThreshold?: number,
}
```

## `hashmap` utility

If you have a list of objects, and want to generate a hashmap from it where each key is the value of a property in that object, you can use the provided `hashmap` function.

Simple Example:

```ts
const stationsList = [
  {
    name: 'Hemannstrasse',
    coords: [12.3839, 56.8327],
  },
  {
    name: 'Rosenthaler Platz',
    coords: [12.8839, 56.37239],
  },
];

const stationsHashmap = hashmap(stationsList, 'name');

// {
//   Hermannstrasse: {
//     name: 'Hermannstrasse',
//     coords: [12.3839, 56.8327],
//   },
//   'Rosenthaler Platz': {
//     name: 'Rosenthaler Platz',
//     coords: [12.8839, 56.37239],
//   },
// };
```

### Additional properties

You can pass additional props which can make multiple entries for each object. This is useful when you have multiple keys that when searched for should return the same object.

```ts
// Additional props are passed in an array, you can pass as many as you want.
const twoPropsHashmap = hashmap(input, 'firstName', ['lastName']);
```

### Nested props

You can access nested props by passing a list of props to access the value at any level within the object.

```ts
const stationsHashmap = hashmap(stationsList, ['properties', 'name']);
```

You can also access nested props in the additional props argument

```ts
// Notice nested alt props are in a list within the list.
const  = hashmap(input, ['properties', 'name'], [['properties', 'altNames']])
```

### When the value at the objects properties is a list

If the value at the provided key is a list, an item is added to the hashmap for every value in the list.

### Type expectations of the value

The value that is being access will be used as the key. Therefore, the only types supported are string and number, which is converted to string. An error will be thrown if you point to a non-string or non-number type.

The property must be present in all of the objects in the list.

Complex Example:

```ts
const stationsList = [
  {
    type: 'Feature',
    geometry: {
      coordinates: [12.3839, 56.8327],
    },
    properties: {
      name: 'Alexanderplatz',
      altNames: ['Alex', 'A-Platz'],
    },
  },
  {
    type: 'Feature',
    geometry: {
      coordinates: [12.123, 56.5849],
    },
    properties: {
      name: 'Rosenthaler Platz',
      altNames: [],
    },
  },
];

const stationsHashmap = hashmap(
  stationsList,
  ['properties', 'name'],
  [['properties', 'altNames']],
);

// {
//   Alexanderplatz: {
//     type: 'Feature',
//     geometry: {
//       coordinates: [12.3839, 56.8327],
//     },
//     properties: {
//       name: 'Alexanderplatz',
//       altNames: ['Alex', 'A-Platz'],
//     },
//   },
//   Alex: {
//     type: 'Feature',
//     geometry: {
//       coordinates: [12.3839, 56.8327],
//     },
//     properties: {
//       name: 'Alexanderplatz',
//       altNames: ['Alex', 'A-Platz'],
//     },
//   },
//   'A-Platz': {
//     type: 'Feature',
//     geometry: {
//       coordinates: [12.3839, 56.8327],
//     },
//     properties: {
//       name: 'Alexanderplatz',
//       altNames: ['Alex', 'A-Platz'],
//     },
//   },
//   'Rosenthaler Platz': {
//     type: 'Feature',
//     geometry: {
//       coordinates: [12.123, 56.5849],
//     },
//     properties: {
//       name: 'Rosenthaler Platz',
//       altNames: [],
//     },
//   },
// }
```

## `createTrigrams` utility

Creates trigrams from a string. It only separates by spaces and includes punctuation in the final result.

```ts
createTrigrams('The sea was angry that day my friends');

// [
//   [ 'The', 'sea', 'was' ],
//   [ 'sea', 'was', 'angry' ],
//   [ 'was', 'angry', 'that' ],
//   [ 'angry', 'that', 'day' ],
//   [ 'that', 'day', 'my' ],
//   [ 'day', 'my', 'friends' ]
// ]
```
